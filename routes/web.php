<?php

// Route::get('/', function () {
// 	return view('index');
// });

Auth::routes();
Route::get('/', 'HomeController@index')->name('index');

//posts get
Route::get('posts', 'PostController@index')->name('posts.index');
Route::get('posts/create', 'PostController@create')->name('posts.create');
Route::get('posts/{post}', 'PostController@show')->name('posts.show');
Route::get('posts/{post}/edit', 'PostController@edit')->name('posts.edit');
//posts post
Route::post('posts', 'PostController@store')->name('posts.store');
Route::put('posts/{id}', 'PostController@update');
Route::delete('posts/{post}', 'PostController@destroy')->name('posts.destroy');
