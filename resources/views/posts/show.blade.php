@extends('layouts.master')
@section('content')
    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <h2 class="section-heading">{{ $post->title }}</h2>
                    <p>{{ $post->body }}</p>
                    <a href="#">
                        <img class="img-responsive" src="{{-- {{ asset('img/post-sample-image.jpg') }} --}}" alt="">
                    </a>
                    <span class="caption text-muted">To go places and do things that have never been done before – that’s what living is all about.</span>
                </div>

                    <a href="{{route('posts.edit',$post->id)}}">
                        <button type="button" class=" btn-sm  btn-group btn-info ">Edit</button></a>
                    {{-- <a href=""><button type="button" class=" btn-sm  btn-danger">Delete</button></a> --}}
                    <a href="{{ route('posts.destroy',$post->id) }}" onclick="event.preventDefault();
                        document.getElementById('delete-form').submit();">
                        {{-- <i class="fa fa-minus" aria-hidden="true"></i> --}}
                        <button type="button" class=" btn-sm  btn-danger">Delete</button>
                      </a>
                      <form id="delete-form" action="{{ route('posts.destroy',$post->id) }}" method="POST"
                       style="display: none;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE')}}
                      </form>

            </div>
        </div>
    </article>
@endsection