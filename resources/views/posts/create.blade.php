@extends('layouts.master')
@section('content')
<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            {{-- Errors validate --}}
                @include('layouts.errors')
            {{-- /Errors --}}

            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <h4>Create a post</h4>
                <a href="/posts">
                <button type="button" class="btn btn-warning">back</button>
            </a>
                <hr>
            </div>
        </div>
        <form method="post" action="/posts" role="form" id="contact-form" class="contact-form col-md-offset-2">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" autocomplete="off" id="title"
                        placeholder="Title" value="{{ old('title') }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <textarea class="form-control textarea" rows="3" name="body" id="body" placeholder="Body content...">{{ old('body') }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <button type="submit" class="btn btn-primary pull-right">create</button>
                </div>
            </div>
        </form>


    </div>
</article>
@endsection