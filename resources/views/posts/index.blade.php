@extends('layouts.master')
@section('content')
@section('css')
<style type="text/css">
.custab{
border: 1px solid #ccc;
padding: 5px;
margin: 5% 0;
box-shadow: 3px 3px 2px #ccc;
transition: 0.5s;
}
.custab:hover{
box-shadow: 3px 3px 0px transparent;
transition: 0.5s;
}
</style>
<link rel="stylesheet" type="text/css" href="<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

@endsection
<!-- Post Content -->
<div class="container">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <h4> posts index</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4>Bootstrap Snipp for Datatable</h4>
            <a href="/posts/create">
                <button type="button" class="btn btn-primary">create post</button>
            </a>

            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                        {{-- <th><input type="checkbox" id="checkall" /></th> --}}
                        <th>#no</th>
                        <th>title</th>
                        <th>body</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </thead>
                    <tbody>

                        <tr>
                            {{-- <td><input type="checkbox" class="checkthis" /></td> --}}
                            {{-- <td>Mohsin</td> --}}
                            <td>1</td>
                            <td>
<?php $str = 'CB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad Pakistan';
if (strlen($str) > 10) {
	echo $str = substr($str, 0, 9) . '...';
}
?>
                            </td>
                            <td>
<?php $str = 'CB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad PakistanCB 106/107 Street # 11 Wah Cantt Islamabad Pakistan';
if (strlen($str) > 81) {
	echo $str = substr($str, 0, 75) . '...';
}
?>
                            </td>
                            <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                            <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
                        </tr>




                    </tbody>
                </table>
                <div class="clearfix"></div>
                <ul class="pagination pull-right">
                    <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input class="form-control " type="text" placeholder="Mohsin">
                    </div>
                    <div class="form-group">
                        <input class="form-control " type="text" placeholder="Irshad">
                    </div>
                    <div class="form-group">
                        <textarea rows="2" class="form-control" placeholder="CB 106/107 Street # 11 Wah Cantt Islamabad Pakistan"></textarea>
                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
@section('js')
<script type="text/javascript">
$(document).ready(function(){
$("#mytable #checkall").click(function () {
if ($("#mytable #checkall").is(':checked')) {
$("#mytable input[type=checkbox]").each(function () {
$(this).prop("checked", true);
});
} else {
$("#mytable input[type=checkbox]").each(function () {
$(this).prop("checked", false);
});
}
});
$("[data-toggle=tooltip]").tooltip();
});
</script>
@endsection
@endsection