@extends('layouts.master')
@section('content')
<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <a href="{{ route('posts.show',$post->id) }}">
                    <button type="submit" class=" btn  btn-group btn-warning ">back</button>
                </a>
                <h2 class="section-heading">{{ $post->title }}</h2>
                <span class="caption text-muted"></span>
            </div>
        </div>

        <form action="{{ route('posts.show',$post->id) }}" method="post" role="form" id="contact-form" class="contact-form col-md-offset-2">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" autocomplete="off" id="title"
                        value="{{ $post->title ?? old('title')}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <textarea class="form-control textarea" rows="3" name="body" id="body"
                        value="{{ $post->body ?? old('body')}}">{{ $post->body ?? old('body')}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">

                    <button type="submit" class="btn btn-success pull-right">update</button>
                </div>
            </div>
        </form>
    </div>
</article>
@endsection