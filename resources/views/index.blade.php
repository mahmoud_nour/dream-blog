@extends('layouts.master')

@section('content')

 <div class="col-lg-8 col-lg-offset-1 col-md-10 col-md-offset-1">
@if ($posts->count() > 0)
    {{-- expr --}}

@foreach ($posts as $post)

     <div class="post-preview">
         <a href="posts/{{ $post->id }}">
             <h2 class="post-title">
                 {{ $post->title }}
             </h2>
             <h3 class="post-subtitle">
                 {{ $post->body }}
             </h3>
         </a>
            <p class="post-meta">on {{ $post->created_at->toFormattedDateString() }}
             </p>
         {{-- <p class="post-meta">Posted by <a href="#">Start Bootstrap</a> on September 24, 2014</p> --}}
     </div>
     <hr class="" style="width: 90%">

@endforeach
     <!-- Pager -->
     <ul class="pager">
         <li class="next">
             <a href="#">Older Posts &rarr;</a>
         </li>
     </ul><!-- Pager -->
@else
<h4 class="col-md-offset-3 text-center">there is no posts to show</h4>
@endif


 </div>

@endsection
