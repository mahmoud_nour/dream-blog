<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller {

	public function index() {

		return view('posts.index');
	}

	public function show(Post $post) {
		// $post = Post::find($id);
		// get /posts/id
		return view('posts.show', compact('post'));
	}

	public function create() {
		return view('posts.create');
	}

	public function edit(Post $post) {
		// get /posts/id/edit
		// return view('posts.edit');
		return view('posts.edit', compact('post'));

	}
	public function store() {
		//Post $post, $request
		// post  /post
		//create new post using request date
		//save it to database
		// redirect to the posts page
		// dd(request()->all());

		// Post::create([
		// 	'title' => request('title'),
		// 	'body' => request('body'),
		// ]);
		$this->validate(request(), [
			'title' => 'required|unique:posts|max:255',
			'body' => 'required',
		]);
		Post::create(request(['title', 'body']));
		return redirect('/posts');
	}

	public function update(Request $request) {
		// PATCH posts/id
		$this->validate(request(), [
			'title' => 'required|max:255',
			'body' => 'required',
		]);

		$post = Post::find($request->id);
		$post->title = $request->title;
		$post->body = $request->body;
		$post->save();
		return view('posts.index');
	}

	public function destroy(Post $post) {
		$post->delete();
		return redirect('/')->with('success', 'Post deleted');
	}
}
